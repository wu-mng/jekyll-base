---
author: anerd
description: This post is a way to test all the possible typographic conventions...
---

### H1-H6

# Header One
## Header Two
### Header Three
#### Header Four
##### Header Five
###### Header Six

### Testing quotes

The following examples may look identical... or not.

"These are dumb quotes"

&ldquo;These are smart quotes&rdquo; <ldquo;> <rdquo;> 

'These are single dumb quotes' <lsquo;> <rsquo;>

&lsquo;These are single smart quotes&rsquo;

In Berlin, by the Wall, you were 5'10" tall...

In Berlin, by the Wall, you were 5&prime;10&Prime; tall...



### Paragraphs

*This is an example of a standard post format.*

Inspiration is a spark. A flash of light. Ignition. But without the proper 
mixture of oxygen and fuel, inspiration both lives and dies in the same instant. 
My life, my experiences, my research; these things are fertile soil for the 
great blog posts hidden within me. I carry them always and they are present 
when I sit down to do my work.

But all too often, it’s a struggle. The raw material is there, but the inspiration 
is not. The oxygen is abundant, but the fuel is scarce. And I’m left wondering, “How?”

> Software is like sex: it's better when it's free.
>> Linus Torvalds

How do I harvest? How do I sift and pan? How do I mine the caverns within me 
for intellectual and emotional gems? How do I… write?

Or perhaps a better question is, how do you? Yes, you, my kindred blog reader. 
Surely, you are similar. Surely, you’ve wrestled the Great Muse down to the dusty earth, and won.

I know this: it starts before I sit down to type. Great posts begin with 
significant life challenges. With weighty problems to solve. With an urgent 
need or a mature discontent with the way things are today. Yes, I think all 
great posts begin before words are written.

> I'm not a big believer in revolutions. What people call revolutions in 
> technology were more of a shift in perception - from big machines to PC's 
> (the technology just evolved, fairly slowly at that), and from PC's to the 
> Internet. The next "revolution" is going to be the same thing - not about 
> the technology itself being revolutionary, but a shift in how you look at it 
> and how you use it.
>> Linus Torvalds

So get up, reader. Walk away from your keyboard, and live.

Only then can you give life… to words.




### Blockquotes

**Single Blockquote**

> Software is like sex: it's better when it's free.
>> Linus Torvalds

**Blockquotes with Multiple Paragraphs**

> I'm not a big believer in revolutions. What people call revolutions in 
> technology were more of a shift in perception - from big machines to PC's 
> (the technology just evolved, fairly slowly at that), and from PC's to the 
> Internet. The next "revolution" is going to be the same thing - not about 
> the technology itself being revolutionary, but a shift in how you look at it 
> and how you use it.
>> Linus Torvalds

### Tables

<table>
  <caption>This is a table caption</caption>
  <tbody>
  <tr>
    <th>Employee</th>
    <th class="views">Salary</th>
    <th></th>
  </tr>
  <tr class="odd">
    <td><a href="http://john.do/">John Saddington</a></td>
    <td>$1</td>
    <td>Because that&#8217;s all Steve Job&#8217; needed for a salary.</td>
  </tr>
  <tr class="even">
    <td><a href="http://tommcfarlin.com/">Tom McFarlin</a></td>
    <td>$100K</td>
    <td>For all the blogging he does.</td>
  </tr>
  <tr class="odd">
    <td><a href="http://jarederickson.com/">Jared Erickson</a></td>
    <td>$100M</td>
    <td>Pictures are worth a thousand words, right? So Tom x 1,000.</td>
  </tr>
  <tr class="even">
    <td><a href="http://chrisam.es/">Chris Ames</a></td>
    <td>$100B</td>
    <td>With hair like that?! Enough said&#8230;</td>
  </tr>
  </tbody>
</table>

### Definition List

<dl>
  <dt>Definition List Title</dt>
    <dd>Definition list division.</dd>
  <dt>Startup</dt>
    <dd>A startup company or startup is a company or temporary organization designed to search for a repeatable and scalable business model.</dd>
  <dt>#dowork</dt>
    <dd>Coined by Rob Dyrdek and his personal body guard Christopher &#8220;Big Black&#8221; Boykins, &#8220;Do Work&#8221; works as a self motivator, to motivating your friends.</dd>
  <dt>Do It Live</dt>
    <dd>I&#8217;ll let Bill O&#8217;Reilly will <a title="We'll Do It Live" href="https://www.youtube.com/watch?v=O_HyZ5aW76c">explain</a> this one.</dd>
</dl>

### Unordered List

- First item
- Second item
- Third item
    - Indented item
    - Indented item
- Fourth item 

### Ordered List

1. First item
2. Second item
3. Third item
    1. Indented item
    2. Indented item
4. Fourth item 

### Other HTML Tags

**\<address>**

<address>
  {{ page.author }} <br> {{ page.contact }}
</address>

**\<abbr>**

The <abbr title="World Health Organization">WHO</abbr> was founded in 1948. 

**\<bdo>**

<bdo dir="rtl">
This text will go right-to-left.
</bdo> 

**\<cite>**

<p><cite>The Scream</cite> by Edward Munch. Painted in 1893.</p> 

**\<code>**

`A piece of computer code`

**\<del>**

My favorite color is ~~blue~~ red 

**\<details>**

 <details>
  <summary>Copyright 1999-2018.</summary>
  <p> - by Refsnes Data. All Rights Reserved.</p>
  <p>All content and graphics on this web site are the property of the company Refsnes Data.</p>
</details> 

**\<dfn>**

<dfn>HTML</dfn> is the standard markup language for creating web pages.

**\<ins>**
My favorite color is ~~blue~~ <ins>red</ins>! 

**\<pre>**

<pre>
This doesn't seem
to work
why?
</pre>

**\<q>**

<q>This is a short quotation.</q> 

**\<s>**

<s>This text is no longer correct</s>

**\<sub>**

use this tag to write formulas, like <sub>H2O</sub>.

**\<sup>**

E=MC<sup>2</sup>, or linked notes[^1]

[^1]: like this one.

**\<template>**

Inspect this element...
<template>
The &lt;template&gt; tag holds its content hidden from the client.
Content inside a &lt;template&gt; tag will not be rendered.
The content can be made visible and rendered later by using JavaScript.
</template>

**\<var>**

<var>Variable</var>
