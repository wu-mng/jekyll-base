This is a minimal Jekyll starter theme, which can be used as base building block 
to jump start a SEO-friendly, standard compliant website.
You should define custom parameters in the _config.yml file.
